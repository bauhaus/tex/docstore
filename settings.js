module.exports = {
  internal: {
    docstore: {
      host: process.env.LISTEN_ADDRESS || "0.0.0.0",
    },
  },
  docstore: {
    backend: process.env.BACKEND || "fs",
    paths: {
      uploadFolder: "/uploads",
    },
  },
};
