#!/usr/bin/env sh
set -e

mkdir -p /uploads
chown -R node /uploads
chmod -R 655 /uploads

exec su-exec node node --expose-gc app.js