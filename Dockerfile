FROM node:14-alpine as base

WORKDIR /app

FROM base as app

RUN apk add --no-cache git python3 make
RUN git clone https://gitlab.com/bauhaus/tex/mirror/docstore.git /app/

RUN npm ci --quiet

FROM base

RUN apk add --no-cache tini su-exec
ENTRYPOINT ["/sbin/tini", "--"]

COPY --from=app /app /app
COPY ./settings.js /app/config/settings.js
COPY ./entrypoint.sh /app/entrypoint.sh

ENV SHARELATEX_CONFIG=/app/config/settings.js

CMD ["./entrypoint.sh"]
